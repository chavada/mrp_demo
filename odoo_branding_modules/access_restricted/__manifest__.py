# -*- coding: utf-8 -*-
{
    'name': 'Restricted administration rights',
    'version': '1.2.0',
    "category": "Access",
    "author": "Madeup Infotech",
    'depends': ['ir_rule_protected'],
    'data': [
        'security/access_restricted_security.xml',
    ],
    'installable': True
}
