# -*- coding: utf-8 -*-
{
    'name': 'Protect ir.rule records',
    'version': '1.0.0',
    'author': 'Madeup Infotech',
    "category": "Access",    
    'depends': [],
    'data': [
        'views.xml',
    ],
    'installable': True
}
