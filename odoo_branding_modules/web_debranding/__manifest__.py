# -*- coding: utf-8 -*-
{
    'name': "Backend debranding",
    'version': '1.0.19',    
    'category': 'Debranding',
    'images': ['images/web_debranding.png'],
    'author': 'Madeup Infotech',    
    'depends': [
        'web',
        'mail',
        'web_planner',
        'access_apps',
        'access_settings_menu',
    ],
    'data': [
        'security/web_debranding_security.xml',
        'security/ir.model.access.csv',
        'data.xml',
        'views.xml',
        'js.xml',
        'pre_install.yml',
    ],
    'qweb': [
        'static/src/xml/web.xml',
        'static/src/xml/extended_chat_window.xml',
    ],
    "post_load": 'post_load',
    'auto_install': False,
    'uninstall_hook': 'uninstall_hook',
    'installable': True
}
