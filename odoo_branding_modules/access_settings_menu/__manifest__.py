# -*- coding: utf-8 -*-
{
    'name': 'Show settings menu for non-admin',
    'version': '1.0.1',
    "category": "Access",
    "author": "Madeup Infotech",
    'depends': [
        'access_apps'
    ],
    'data': [
        'security/access_settings_menu_security.xml',
    ],
    'installable': True
}
