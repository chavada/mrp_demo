# -*- coding: utf-8 -*-

from heapq import nsmallest

from odoo import api, fields, models, _
from odoo.tools.translate import _
from odoo.exceptions import UserError

import logging

_logger = logging.getLogger(__name__)

RentalDurationUnits = [
    ('months', 'Month(s)'),
    ('days', 'Day(s)'),
    ('weeks', 'Week(s)'),
    ('years', 'Year(s)'),
    ('hours', 'Hour(s)'),
    ('minutes', 'Minute(s)'),
]


# relativedelta supports following:
#     self.years = 0
#     self.months = 0
#     self.days = 0
#     self.leapdays = 0
#     self.hours = 0
#     self.minutes = 0
#     self.seconds = 0
#     self.microseconds = 0
#     self.year = None
#     self.month = None
#     self.day = None
#     self.weekday = None
#     self.hour = None
#     self.minute = None
#     self.second = None
#     self.microsecond = None
#     self._has_time = 0


def get_tenure_value_price_tuple_list(my_dict, tenure_value, tenure_value_price_tuple_list=[]):
    if my_dict.get(tenure_value, False):
        tenure_value_price_tuple_list.append(
            (tenure_value, my_dict.get(tenure_value, 0.0)))
        return tenure_value_price_tuple_list
    nearest_num_list = nsmallest(
        2, my_dict, key=lambda x: abs(x - tenure_value))
    if nearest_num_list:
        if tenure_value >= nearest_num_list[0]:
            tenure_value_price_tuple_list.append(
                (nearest_num_list[0], my_dict.get(nearest_num_list[0], 0.0)))
            tenure_value = tenure_value - nearest_num_list[0]
            get_tenure_value_price_tuple_list(
                my_dict, tenure_value, tenure_value_price_tuple_list)
            return tenure_value_price_tuple_list
        elif tenure_value < nearest_num_list[0] and len(nearest_num_list) == 2:
            if tenure_value >= nearest_num_list[1]:
                tenure_value_price_tuple_list.append(
                    (nearest_num_list[1], my_dict.get(nearest_num_list[1], 0.0)))
                tenure_value = tenure_value - nearest_num_list[1]
                get_tenure_value_price_tuple_list(
                    my_dict, tenure_value, tenure_value_price_tuple_list)
                return tenure_value_price_tuple_list


class RentalProductTemplate(models.Model):
    """ Inherit product.template modal to adding addition requirements for rental product """

    # Private attributes
    _inherit = 'product.template'

    # Default methods

    # Fields declaration
    rental_ok = fields.Boolean("Available for Rent")
    rental_tenure_id = fields.Many2one(
        "product.rental.tenure",
        "Rental Tenure Starting",
    )
    rental_tenure_ids = fields.One2many(
        "product.rental.tenure",
        "product_tmpl_id",
        "Rental Tenures",
    )
    description_rental = fields.Char("Rental Description")
    security_amount = fields.Float("Security Amount")
    tenure_type_standard = fields.Boolean(
        "Standard Tenure",
        default=True,
    )
    tenure_type_custom = fields.Boolean("Custom Tenure")
    rental_agreement_id = fields.Many2one(
        "rental.product.agreement",
        "Product Agreement",
    )
    rental_categ_id = fields.Many2one(
        "rental.product.category",
        "Rental Category",
    )

    # compute and search fields, in the same order of fields declaration

    # Constraints and onchanges

    # CRUD methods (and name_get, name_search, pass) overrides

    @api.model
    def calculate_kf(self, tenure_value):
        kf = 0

        amount = self.calculate_one_shot_value(tenure_value)[0] + self.calculate_one_shot_value(tenure_value)[
            1] + self.list_price
        _logger.warning('-----------------amount %s', amount)
        month = tenure_value
        if amount >= 300 and amount <= 3000:
            if month == 13:
                kf = 8.289
            elif month == 24:
                kf = 4.797
            elif month == 36:
                kf = 3.448
            elif month == 48:
                kf = 2.782
            elif month == 60:
                kf = 2.389
        elif amount >= 3001 and amount <= 6000:
            if month == 13:
                kf = 8.208
            elif month == 24:
                kf = 4.71
            elif month == 36:
                kf = 3.356
            elif month == 48:
                kf = 2.686
            elif month == 60:
                kf = 2.289
        elif amount >= 6001 and amount <= 10000:
            if month == 13:
                kf = 8.128
            elif month == 24:
                kf = 4.624
            elif month == 36:
                kf = 3.266
            elif month == 48:
                kf = 2.591
            elif month == 60:
                kf = 2.19
        elif amount >= 10001 and amount <= 20000:
            if month == 13:
                kf = 8.088
            elif month == 24:
                kf = 4.581
            elif month == 36:
                kf = 3.221
            elif month == 48:
                kf = 2.545
            elif month == 60:
                kf = 2.142
        elif amount >= 20001 and amount <= 999999999:
            if month == 13:
                kf = 8.048
            elif month == 24:
                kf = 4.539
            elif month == 36:
                kf = 3.176
            elif month == 48:
                kf = 2.498
            elif month == 60:
                kf = 2.094
        _logger.warning('-----------------kf %s', kf)
        value = (kf * (
            self.list_price + self.calculate_one_shot_value(tenure_value)[0] +
            self.calculate_one_shot_value(tenure_value)[
                1])) / 100
        _logger.warning('-----------------value %s', value)
        total_value = value + (
            self.list_price + self.calculate_one_shot_value(tenure_value)[0] +
            self.calculate_one_shot_value(tenure_value)[
                1])
        _logger.warning('-----------------total_value %s', total_value)
        base_rental_price_value = (self.list_price + self.calculate_one_shot_value(tenure_value)[0])
        _logger.warning('----------|||||-------base_rental_price_value %s', base_rental_price_value)
        kf_ta = (round(month / 12) * kf * (
             self.list_price + self.calculate_one_shot_value(tenure_value)[0] +
             self.calculate_one_shot_value(tenure_value)[
                 1])) / 100
        prix_de_base = (
            self.list_price + self.calculate_one_shot_value(tenure_value)[0] +
            self.calculate_one_shot_value(tenure_value)[
                1])
        assurance = self.calculate_one_shot_value(tenure_value)[2]
        _logger.warning('----------|||||-------assurance %s', assurance)
        _logger.warning('----------|||||-------prix_de_base %s', prix_de_base)
        _logger.warning('----------|||||-------kf_ta %s', kf_ta)
        return [base_rental_price_value, total_value, value, kf_ta, prix_de_base, assurance]

    @api.model
    def calculate_one_shot_value(self, tenure_value):
        one_shoot = 0
        annuelle = 0
        assurance = 0
        _logger.warning('-----------------self.rental_categ_id.low_cost %s', self.rental_categ_id.low_cost)
        _logger.warning('-----------------self.rental_categ_id.medium_cost %s', self.rental_categ_id.medium_cost)
        _logger.warning('-----------------self.rental_categ_id.premium_cost %s', self.rental_categ_id.low_cost)
        _logger.warning('-----------------self.list_price %s', self.list_price)
        if self.list_price < self.rental_categ_id.medium_cost:
            _logger.warning('-----------------calculate_one_shot_value IF-1')
            one_shoot = self.list_price * (self.rental_categ_id.l7_total / 100)
            annuelle = (self.list_price * (self.rental_categ_id.l1_locasion / 100)) * round(tenure_value / 12)
            assurance = (self.list_price * tenure_value * self.rental_categ_id.l5_assurance) / 100
        elif self.list_price >= self.rental_categ_id.medium_cost and self.list_price < self.rental_categ_id.premium_cost:
            _logger.warning('-----------------calculate_one_shot_value IF-2')
            one_shoot = self.list_price * (self.rental_categ_id.m7_total / 100)
            annuelle = (self.list_price * (self.rental_categ_id.m1_locasion / 100)) * round(tenure_value / 12)
            assurance = (self.list_price * tenure_value * self.rental_categ_id.m5_assurance) / 100
        elif self.list_price >= self.rental_categ_id.premium_cost:
            _logger.warning('-----------------calculate_one_shot_value IF-3')
            one_shoot = self.list_price * (self.rental_categ_id.p7_total / 100)
            annuelle = (self.list_price * (self.rental_categ_id.p1_locasion / 100)) * round(tenure_value / 12)
            assurance = (self.list_price * tenure_value * self.rental_categ_id.p5_assurance) / 100
        _logger.warning('-----------------one_shoot %s annuelle %s assurance %s', one_shoot, annuelle, assurance)
        return [one_shoot, annuelle, assurance]


    def button_calculate_rental(self):
        if self.rental_categ_id:
            n = 0
            product_uom = self.env['product.uom'].search([('duration_unit', '=', 'months')], limit=1)
            _logger.warning('-----------------product_uom %s', product_uom)
            for record in self.rental_tenure_ids:
                record.unlink()
            if self.rental_categ_id.max_num_mo == 13:
                n = 1
            elif self.rental_categ_id.max_num_mo == 24:
                n = 2
            elif self.rental_categ_id.max_num_mo == 36:
                n = 3
            elif self.rental_categ_id.max_num_mo == 48:
                n = 4
            elif self.rental_categ_id.max_num_mo == 60:
                n = 5
            line = 0
            tenure_value = 0
            rent_price = 0
            # vrednost = fefok[0]
            for rec in range(n):
                if line == 0:
                    tenure_value = 13
                elif line == 1:
                    tenure_value = 24
                elif line == 2:
                    tenure_value = 36
                elif line == 3:
                    tenure_value = 48
                elif line == 4:
                    tenure_value = 60
                rent_price += 100
                #self.rental_tenure_ids.write
                self.env['product.rental.tenure'].create({
                    'product_tmpl_id': self.id,
                    'tenure_value': tenure_value,
                    'is_default': True,
                    'rent_price': (self.calculate_kf(tenure_value)[5] + self.calculate_kf(tenure_value)[4] +
                                   self.calculate_kf(tenure_value)[3]) / tenure_value,
                    'rental_uom_id': product_uom.id
                })
                vrednost1 = self.calculate_kf(tenure_value)[5]
                vrednost2 = self.calculate_kf(tenure_value)[4]
                vrednost3 = self.calculate_kf(tenure_value)[3]
                vrednost4 = tenure_value
                _logger.warning('-----------------vrednost 1 = %s vrednost 2 = %s vrednost 3 = %s vrednost 4 = %s', vrednost1, vrednost2, vrednost3, vrednost4)
                tenure_value += 12
                line += 1
            rental_tenure_id = False
            for tenure in self.rental_tenure_ids:
                rental_tenure_id = tenure.id
            self.rental_tenure_id = rental_tenure_id
    # Action methods
    @api.multi
    def action_validate(self):
        self.ensure_one()
        pass

    # Business methods

    @api.multi
    def get_product_tenure_price(self, tenure_value, tenure_uom_id):
        self.ensure_one()
        return_value_price_pair = []
        my_dict = self.get_rental_tenure_with_price(tenure_uom_id)
        rental_calculated_for = 0
        calculated_rental_price = 0
        if self.rental_ok:
            if my_dict and tenure_value:
                if self.env['ir.default'].sudo().get('res.config.settings',
                                                     'rental_rate_calculation_method') == "simple":
                    rental_calculated_for = 0.0
                    calculated_rental_price = 0.0
                else:
                    list_of_pairs = get_tenure_value_price_tuple_list(
                        my_dict, tenure_value, [])
                    if list_of_pairs:
                        return_value_price_pair = [sum(x) for x in zip(*list_of_pairs)]
                        if return_value_price_pair:
                            rental_calculated_for = return_value_price_pair[0]
                            calculated_rental_price = return_value_price_pair[1]
                if tenure_value > rental_calculated_for:
                    keys_tenure = my_dict.keys()
                    if keys_tenure:
                        min_tenure = min(keys_tenure)
                        price_for_min_tenure = my_dict.get(min_tenure, 0.0)
                        unit_rent_price = price_for_min_tenure / min_tenure
                        remaning_tenure = tenure_value - rental_calculated_for
                        remaning_tenure_amount = remaning_tenure * unit_rent_price
                        # update return_value_price_pair
                        temp_tenure = rental_calculated_for + \
                                      remaning_tenure
                        temp_price = calculated_rental_price + \
                                     remaning_tenure_amount
                        if return_value_price_pair:
                            return_value_price_pair[0] = temp_tenure
                            return_value_price_pair[1] = temp_price
                        else:
                            return_value_price_pair.append(temp_tenure)
                            return_value_price_pair.append(temp_price)
                            # Write message for how much time rent calculated
        return return_value_price_pair

    @api.model
    def get_applicable_rental_uom_ids(self):
        duration_list = []
        if self.rental_ok and self.rental_tenure_ids:
            for obj in self.rental_tenure_ids:
                duration_list.append(obj.rental_uom_id.id)
        return duration_list

    @api.model
    def get_rental_tenure_with_price(self, rental_uom):
        my_dict = {}
        if self.rental_ok:
            for rental_tenure_scheme in self.rental_tenure_ids.filtered(lambda r: r.rental_uom_id.id == rental_uom):
                my_dict.update({
                    rental_tenure_scheme.tenure_value: rental_tenure_scheme.rent_price
                })
            return my_dict

    @api.model
    def create(self, vals):
        res = super(RentalProductTemplate, self).create(vals)
        if vals.get('rental_tenure_ids'):
            if not vals.get('rental_tenure_ids'):
                raise UserError('Please add atleast one Rental Tenure')
        return res


class ProductProduct(models.Model):
    """ Inherit product.product modal to adding addition requirements for rental product """

    # Private attributes
    _inherit = 'product.product'

    # Business methods

    @api.multi
    def get_product_tenure_price(self, tenure_value, tenure_uom_id):
        self.ensure_one()
        return self.product_tmpl_id.get_product_tenure_price(tenure_value, tenure_uom_id)

    @api.model
    def get_applicable_rental_uom_ids(self):
        return self.product_tmpl_id.get_applicable_rental_uom_ids()

    @api.model
    def get_rental_tenure_with_price(self, rental_uom):
        return self.product_tmpl_id.get_rental_tenure_with_price(rental_uom)


class ProductRentalTenure(models.Model):
    """ This modal is design to manage rental tenure for rental product"""

    # Private attributes
    _name = 'product.rental.tenure'
    _description = "Rental product rent tenures"

    # Fields declaration
    name = fields.Char(string="Tenure", compute="_compute_name")
    tenure_value = fields.Float("Tenure", default=1.0, required=True)
    max_tenure_value = fields.Float("Max. Tenure")
    rental_uom_id = fields.Many2one("product.uom", "Tenure UOM", required=True,
                                    domain=lambda self: [('is_rental_uom', '=', True)])
    rent_price = fields.Float("Rent Price")
    is_default = fields.Boolean("Default Tenure")
    product_tmpl_id = fields.Many2one("product.template", "Product Template")
    currency_id = fields.Many2one(related="product_tmpl_id.currency_id", string="Currency")

    # rental_tenure_type = fields.Selection(
    #     [('standard', 'Standard Tenure'), ('custom', 'Custom Tenure')], "Rental Tenure Type", default="standard")





    # compute and search fields, in the same order of fields declaration
    @api.multi
    @api.depends('product_tmpl_id', 'rental_uom_id')
    def _compute_name(self):
        for rec in self:
            name = "For " + str(rec.tenure_value) + " " + \
                   rec.rental_uom_id.name
            price_with_currency = ""
            if rec.currency_id and rec.currency_id.position == "before":
                price_with_currency = str(
                    rec.currency_id.symbol) + str(rec.rent_price)
            else:
                price_with_currency = str(
                    rec.rent_price) + str(rec.currency_id.symbol)
            rec.name = name + " @ " + price_with_currency

    # Constraints and onchanges
    @api.constrains('max_tenure_value')
    def _check_max_tenure_value(self):
        if self.rental_uom_id and self.max_tenure_value <= 0:
            raise UserError(_("Max Tenure Value for %r must be greter than zero." %
                              self.rental_uom_id.name))

    @api.onchange('max_tenure_value')
    def _onchange_max_tenure_value(self):
        if self.rental_uom_id and self.max_tenure_value <= 0:
            raise UserError(
                _("Max Tenure Value for %r must be greater than zero." % self.rental_uom_id.name))

    # Constraints and onchanges
    @api.constrains('rent_price')
    def _check_rent_price(self):
        if self.rental_uom_id and self.rent_price <= 0:
            raise UserError(_("Rent price for %r must be greter than zero." %
                              self.rental_uom_id.name))

    @api.onchange('rent_price')
    def _onchange_rent_price(self):
        if self.rental_uom_id and self.rent_price <= 0:
            raise UserError(
                _("Rent price for %r must be greter than zero." % self.rental_uom_id.name))

            # CRUD methods (and name_get, name_search, pass) overrides

            # Business methods


class RentalProductAgreement(models.Model):
    """ This modal is design to manage rental product agreement for rental product"""

    # Private attributes
    _name = 'rental.product.agreement'

    # Fields declaration
    name = fields.Char("Name", required=True)
    sequence = fields.Integer("Sequence", required=True)
    description = fields.Text(string="Description")
    agreement_file = fields.Binary("Product Agreement")

    filename = fields.Char('file name', readonly=True, store=False, compute='_getFilename')

    @api.multi
    def _getFilename(self):
        for rec in self:
            rec.filename = rec.name[:10] + '.pdf'


class ProductUom(models.Model):
    # Private attributes
    _inherit = 'product.uom'

    # Fields declaration

    is_rental_uom = fields.Boolean("Rental UOM")
    duration_unit = fields.Selection(RentalDurationUnits, "Rental Unit")

    _sql_constraints = [
        ('unique_uom_duration_unit', 'unique(duration_unit)',
         _('There is already a record with this duration unit.'))
    ]


class RentalProductCategory(models.Model):
    """ This modal is design to manage rental category for rental product"""

    # Private attributes
    _name = 'rental.product.category'

    # Fields declaration

    name = fields.Char(string="Name", required=True, translate=True)
    hide_all_product = fields.Boolean("Hide Category from Website",
                                      help="Enable to hide this category and all products belong to this category from website.")
    product_count = fields.Integer(
        '# Products', compute='_compute_product_count',
        help="The number of products under this rental category.")
    image = fields.Binary("Category Image")
    # dodadatak za koeficijente obracuna osnovne cene za rentiranje

    max_num_mo = fields.Integer(string="The maximum number of months on which the equipment can be rented ")

    # low cost
    low_cost = fields.Float(string='Low Cost', required=True)
    l1_locasion = fields.Float(string="Low cost Visite (%)", required=False, translate=True, store=True,
                               compute="_compute_low_cost")
    l2_intallation = fields.Float(string="Low cost Installation (%)", required=False, translate=True, store=True,
                                  compute="_compute_low_cost")
    l3_maintenance = fields.Float(string="Low cost Démontage (%)", required=False, translate=True, store=True,
                                  compute="_compute_low_cost")
    l4_controle = fields.Float(string="Low cost Epreuve (%)", required=False, translate=True, store=True,
                               compute="_compute_low_cost")
    l5_assurance = fields.Float(string="Low cost Assurance (%)", required=True, translate=True)
    l6_livrasion = fields.Float(string="Low cost Livraison (%)", required=False, translate=True, store=True,
                                compute="_compute_low_cost")
    l7_total = fields.Float(string="Low cost Total (%)", required=False, translate=True, store=True,
                            compute="_compute_low_cost")

    # medium
    medium_cost = fields.Float(string='Medium', required=True)
    m1_locasion = fields.Float(string="Medium Visite (%)", required=False, translate=True, store=True,
                               compute="_compute_medium_cost")
    m2_intallation = fields.Float(string="Medium Installation (%)", required=False, translate=True, store=True,
                                  compute="_compute_medium_cost")
    m3_maintenance = fields.Float(string="Medium Démontage (%)", required=False, translate=True, store=True,
                                  compute="_compute_medium_cost")
    m4_controle = fields.Float(string="Medium Epreuve (%)", required=False, translate=True, store=True,
                               compute="_compute_medium_cost")
    m5_assurance = fields.Float(string="Medium Assurance (%)", required=True, translate=True)
    m6_livrasion = fields.Float(string="Medium Livraison (%)", required=False, translate=True, store=True,
                                compute="_compute_medium_cost")
    m7_total = fields.Float(string="Medium cost Total (%)", required=False, translate=True, store=True,
                            compute="_compute_medium_cost")
    # premium
    premium_cost = fields.Float(string='Premium', required=True)
    p1_locasion = fields.Float(string="Premium Visite (%)", required=False, translate=True, store=True,
                               compute="_compute_premium_cost")
    p2_intallation = fields.Float(string="Premium Installation (%)", required=False, translate=True, store=True,
                                  compute="_compute_premium_cost")
    p3_maintenance = fields.Float(string="Premium Démontage (%)", required=False, translate=True, store=True,
                                  compute="_compute_premium_cost")
    p4_controle = fields.Float(string="Premium Epreuve (%)", required=False, translate=True, store=True,
                               compute="_compute_premium_cost")
    p5_assurance = fields.Float(string="Premium Assurance (%)", required=True, translate=True)
    p6_livrasion = fields.Float(string="Premium Livraison (%)", required=False, translate=True, store=True,
                                compute="_compute_premium_cost")
    p7_total = fields.Float(string="Premium cost Total (%)", required=False, translate=True, store=True,
                            compute="_compute_premium_cost")
    # prix moyen
    prix_locasion = fields.Float(string="Prix Visite (%)", required=True, translate=True)
    prix_intallation = fields.Float(string="Prix Installation (%)", required=True, translate=True)
    prix_maintenance = fields.Float(string="Prix Démontage (%)", required=True, translate=True)
    prix_controle = fields.Float(string="Prix Epreuve (%)", required=True, translate=True)
    prix_assurance = fields.Float(string="Prix Assurance (%)", required=False, translate=True)
    prix_livrasion = fields.Float(string="Prix Livraison (%)", required=True, translate=True)
    prix_total = fields.Float(string="Prix cost Total (%)", required=True, translate=True)

    ksum = fields.Float(string="Collecting coefficient", compute="_compute_ksum",
                        help="The coefficient used to calculate the base rental price")

    @api.multi
    def button_calculate_all_rental(self):
        category_in_products = self.env["product.template"].search([("rental_categ_id", '=', self.id)])
        for record in category_in_products:
            record.button_calculate_rental()

    @api.multi
    @api.onchange('prix_intallation', 'prix_livrasion', 'prix_maintenance', 'prix_controle')
    def _calculate_prix_total(self):
        for cost in self:
            cost.prix_total = cost.prix_intallation + cost.prix_livrasion + cost.prix_maintenance + cost.prix_controle

    # compute low cost
    @api.depends('low_cost', 'medium_cost', 'premium_cost', 'prix_livrasion', 'prix_locasion', 'prix_intallation', 'prix_maintenance', 'prix_controle', 'prix_total')
    def _compute_low_cost(self):
        for cost in self:
            if cost.low_cost > 0:
                cost.l6_livrasion = round((cost.prix_livrasion / cost.low_cost) * 100)
                cost.l1_locasion = round((cost.prix_locasion / cost.low_cost) * 100)
                cost.l2_intallation = round((cost.prix_intallation / cost.low_cost) * 100)
                cost.l3_maintenance = round((cost.prix_maintenance / cost.low_cost) * 100)
                cost.l4_controle = round((cost.prix_controle / cost.low_cost) * 100)
                # cost.l5_assurance = round((cost.prix_assurance / cost.low_cost) * 100)
                cost.l7_total = round((cost.prix_total / cost.low_cost) * 100)

    # compute medium

    @api.depends('low_cost', 'medium_cost', 'premium_cost', 'prix_livrasion', 'prix_locasion', 'prix_intallation',
                 'prix_maintenance', 'prix_controle', 'prix_total')
    def _compute_medium_cost(self):
        for cost in self:
            if cost.medium_cost > 0:
                cost.m1_locasion = round((cost.prix_locasion / cost.medium_cost) * 100)
                cost.m2_intallation = round((cost.prix_intallation / cost.medium_cost) * 100)
                cost.m3_maintenance = round((cost.prix_maintenance / cost.medium_cost) * 100)
                cost.m4_controle = round((cost.prix_controle / cost.medium_cost) * 100)
                # cost.m5_assurance = round((cost.prix_assurance / cost.medium_cost) * 100)
                cost.m6_livrasion = round((cost.prix_livrasion / cost.medium_cost) * 100)
                cost.m7_total = round((cost.prix_total / cost.medium_cost) * 100)

    # compute premium

    @api.depends('low_cost', 'medium_cost', 'premium_cost', 'prix_livrasion', 'prix_locasion', 'prix_intallation',
                 'prix_maintenance', 'prix_controle', 'prix_total')
    def _compute_premium_cost(self):
        for cost in self:
            if cost.premium_cost > 0:
                cost.p1_locasion = round((cost.prix_locasion / cost.premium_cost) * 100)
                cost.p2_intallation = round((cost.prix_intallation / cost.premium_cost) * 100)
                cost.p3_maintenance = round((cost.prix_maintenance / cost.premium_cost) * 100)
                cost.p4_controle = round((cost.prix_controle / cost.premium_cost) * 100)
                # cost.p5_assurance = round((cost.prix_assurance / cost.premium_cost) * 100)
                cost.p6_livrasion = round((cost.prix_livrasion / cost.premium_cost) * 100)
                cost.p7_total = round((cost.prix_total / cost.premium_cost) * 100)

    # compute and search fields, in the same order of fields declaration

    def _compute_product_count(self):
        for categ in self:
            categ.product_count = self.env["product.template"].search_count([("rental_categ_id", '=', categ.id)])

    def _compute_ksum(self):

        self.ksum = self.l1_locasion + self.l2_intallation + self.l3_maintenance + self.l4_controle + self.l5_assurance + self.l6_livrasion
